name := "checkersOneModule"

version := "0.1"

scalaVersion := "2.12.12"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.3" % Test

libraryDependencies += "io.spray" %% "spray-json" % "1.3.6"

libraryDependencies += "com.typesafe" % "config" % "1.4.1"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.1"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion
)
