package checkers.core

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import spray.json.JsValue

class SerializationTest extends AnyFlatSpec with should.Matchers{
  val initialBoard = Board(
    Seq(
      Seq(Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1)),
      Seq(TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty),
      Seq(Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1)),
      Seq(Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty),
      Seq(Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty),
      Seq(TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty),
      Seq(Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2)),
      Seq(TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty)
    )
  )

  "Serializer" should "serialize and deserialize correctly" in {
    val serialized: JsValue = Serializer.serializeGame(Replay(List(State(initialBoard))))

    val deserialized = Serializer.deserializeGame(serialized)

    Replay(List(State(initialBoard))) should equal(deserialized)
  }
}
