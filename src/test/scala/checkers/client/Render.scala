package checkers.client

import checkers.core._
import org.scalatest._
import flatspec._
import matchers._

class RenderTest extends AnyFlatSpec with should.Matchers {
  it should "render to string" in {
    val initialBoard = Board(
      Seq(
        Seq(Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1)),
        Seq(TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty),
        Seq(Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1)),
        Seq(Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty),
        Seq(Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty),
        Seq(TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty),
        Seq(Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2)),
        Seq(TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty)
      )
    )
    val renderer = new RenderString
    val boardString = renderer.render(State(initialBoard))
    println(boardString)
    boardString should equal("    1   2   3   4   5   6   7   8\n1  |  ||1o||  ||1o||  ||1o||  ||1o|\n2  |1o||  ||1o||  ||1o||  ||1o||  |\n3  |  ||1o||  ||1o||  ||1o||  ||1o|\n4  |  ||  ||  ||  ||  ||  ||  ||  |\n5  |  ||  ||  ||  ||  ||  ||  ||  |\n6  |2o||  ||2o||  ||2o||  ||2o||  |\n7  |  ||2o||  ||2o||  ||2o||  ||2o|\n8  |2o||  ||2o||  ||2o||  ||2o||  |")
  }
}
