package checkers.core

import scala.util.Try

import spray.json._

trait Tile
case object Empty extends Tile

case class TileWithPiece(piece: Piece, player: Player) extends Tile

trait Piece
case object Pawn extends Piece
case object Queen extends Piece

trait Player
case object Player1 extends Player
case object Player2 extends Player

case class Board(board: Seq[Seq[Tile]])

case class State(board: Board)
case class Replay(game: Seq[State])

case class Move(from: (Int, Int), to: (Int, Int))

object Checkers{
  def makeMove(move: Move, state: State): Try[State] = Try{
    val arrayOfArrays: Array[Array[Tile]] = state.board.board.map{_.toArray}.toArray

    val tile = arrayOfArrays(move.from._1)(move.from._2)
    arrayOfArrays(move.from._1)(move.from._2) = Empty
    arrayOfArrays(move.to._1)(move.to._2) = tile

    State(Board(arrayOfArrays.map(_.toSeq).toSeq))
  }
}

object JsonSupport{
  import checkers.core.serialization.CaseObjectSerializationSupport._

  implicit val pawnFormat = caseObjectJsonFormat(Pawn)
  implicit val queenFormat = caseObjectJsonFormat(Queen)
  implicit val player1Format = caseObjectJsonFormat(Player1)
  implicit val player2Format = caseObjectJsonFormat(Player2)

  implicit object PieceFormat extends JsonFormat[Piece]{
    override def write(obj: Piece): JsValue = obj match {
      case Queen => Queen.toJson
      case Pawn => Pawn.toJson
    }

    override def read(json: JsValue): Piece = json match{
      case JsString("Queen") => Queen
      case JsString("Pawn") => Pawn
    }
  }

  implicit object PlayerFormat extends JsonFormat[Player]{
    override def write(obj: Player): JsValue = {
      obj match {
        case Player1 => Player1.toJson
        case Player2 => Player2.toJson
      }
    }

    override def read(json: JsValue): Player = json match{
      case JsString("Player1") => Player1
      case JsString("Player2") => Player2
    }
  }

  implicit val tileWithPieceFormat = jsonFormat2(TileWithPiece)

  implicit object TileFormat extends JsonFormat[Tile]{
    override def read(json: JsValue): Tile = json match {
      case JsString("Empty") => Empty
      case _ => json.convertTo[TileWithPiece]
    }

    override def write(obj: Tile): JsValue = {
      obj match {
        case t:TileWithPiece => t.toJson
        case Empty => JsString("Empty")
      }
    }
  }

  implicit val boardFormat = jsonFormat1(Board)
  implicit val gameStateFormat = jsonFormat1(State)
  implicit val gameFormat = jsonFormat1(Replay)
}


object Serializer{
  import JsonSupport._

  def serializeGame(game: Replay): JsValue ={
    game.toJson
  }

  def deserializeGame(gameJson: JsValue): Replay = {
    gameJson.convertTo[Replay]
  }
}
