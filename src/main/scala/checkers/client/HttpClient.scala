package checkers.client

import java.io.{ByteArrayOutputStream, ObjectOutputStream}

import scala.concurrent.ExecutionContext

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest}
import checkers.core.State
import com.typesafe.config.ConfigFactory

object HttpClient extends App{
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = actorSystem.dispatcher
  private val config = ConfigFactory.load()
  private val saveGameUrl = config.getString("server-url-save-game")


/*  def saveGame(game: List[GameState]): Unit ={
    val serializedGame = serializeGame(game)
    HttpRequest(
      method = HttpMethods.POST,
      uri = saveGameUrl,
      entity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, serializedGame)
    )
  }*/
}
