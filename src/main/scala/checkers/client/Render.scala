package checkers.client

import checkers.core._

trait Render {
  def render(gameState: State): Any
}

class RenderString extends Render {
  override def render(gameState: State): String = {
    "    "  + "1" + "   " + "2" + "   " + "3" + "   " + "4" + "   " + "5" + "   " + "6" + "   " + "7" + "   " + "8" + "\n" +
    gameState.board.board.zipWithIndex.map{ case (row, index) =>
      (index + 1).toString + "  " +
      row.map{
        case TileWithPiece(Pawn, Player1) => "|1o|"
        case TileWithPiece(Pawn, Player2) => "|2o|"
        case TileWithPiece(Queen, Player1) => "|1*|"
        case TileWithPiece(Queen, Player2) => "|2*|"
        case Empty => "|  |"
      }.mkString
    }.mkString("\n")
  }
}

/*object Main extends App {
  val initialBoard = Board(
    List(
      List(Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1)),
      List(TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty),
      List(Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1), Empty, TileWithPiece(Pawn, Player1)),
      List(Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty),
      List(Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty),
      List(TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty),
      List(Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2)),
      List(TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty, TileWithPiece(Pawn, Player2), Empty)
    )
  )

  val renderer = new RenderString
  renderer.render(State(initialBoard))
}*/



